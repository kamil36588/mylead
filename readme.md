<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

# Laravel v6.5.0

**Instalation**:

1. Clone repository <br />
2. Install composer dependencies by running: `composer install` in root directory of project <br />
3. Copy environment config, from `.env.default` to `.env`, and change it for your credentials. <br />
4. Create database schema, by command `php artisan migrate` <br />


###### Have fun at work!
